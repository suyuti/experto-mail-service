const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    toplantimsid  : String,
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports =
  mongoose.models.Toplanti || mongoose.model("Toplanti", SchemaModel);
