const axios = require("axios");
var querystring = require("querystring");
const Toplanti = require("./models/toplanti");

const tenantId = process.env.TENANT_ID;
const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;

const createEvent = async (fromMsId, toMsId, eventObj) => {
  try {
    const respLogin = await axios.post(
      `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
      querystring.stringify({
        client_id: clientId,
        scope: "https://graph.microsoft.com/.default",
        client_secret: clientSecret,
        grant_type: "client_credentials",
      }),
      {
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
      }
    );

    const to = await axios.get(
      `https://graph.microsoft.com/v1.0/users/${toMsId}`,
      { headers: { Authorization: "Bearer " + respLogin.data.access_token } }
    );

    var eventParam = {
      subject: `${eventObj.subject}`,
      body: {
        contentType: "HTML",
        content: `${eventObj.content}`,
      },
      start: {
        dateTime: `${eventObj.startDate}`, //"2019-03-15T12:00:00",
        timeZone: "Turkey Standard Time",
      },
      end: {
        dateTime: `${eventObj.endDate}`, //"2019-03-15T14:00:00",
        timeZone: "Turkey Standard Time",
      },
      location: {
        displayName: `${eventObj.location}`,
      },
      attendees: [
        //{
        //  emailAddress: {
        //    address: "mehmet.dindar@experto.com.tr",
        //    name: "Adele Vance",
        //  },
        //  type: "required",
        //},
      ],
      isOnlineMeeting: eventObj.isOnline,
      onlineMeetingProvider: "teamsForBusiness",
    };

    eventObj.katilimcilar.map((k) => {
      eventParam.attendees.push({
        emailAddress: {
          address: k.mail,
          name: k.adi,
        },
        type: "required",
      });
    });

    const c = await axios.post(
      `https://graph.microsoft.com/v1.0/users/${fromMsId}/calendar/events`,
      eventParam,
      { headers: { Authorization: "Bearer " + respLogin.data.access_token } }
    );
    console.log(`Toplantı daveti gonderildi`);

    let t = await Toplanti.findByIdAndUpdate(eventObj._id, {
      $set: { toplantimsid: c.data.id },
    });
    //console.log(t);
  } catch (e) {
    console.log(`Toplanti daveti hata `, e);
  }
};

const updateEvent = async (fromMsId, toMsId, eventObj) => {
  try {
    const respLogin = await axios.post(
      `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
      querystring.stringify({
        client_id: clientId,
        scope: "https://graph.microsoft.com/.default",
        client_secret: clientSecret,
        grant_type: "client_credentials",
      }),
      {
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
      }
    );

    const to = await axios.get(
      `https://graph.microsoft.com/v1.0/users/${toMsId}`,
      { headers: { Authorization: "Bearer " + respLogin.data.access_token } }
    );

    var eventParam = {
      subject: `${eventObj.subject}`,
      body: {
        contentType: "HTML",
        content: `${eventObj.content}`,
      },
      start: {
        dateTime: `${eventObj.startDate}`, //"2019-03-15T12:00:00",
        timeZone: "Turkey Standard Time",
      },
      end: {
        dateTime: `${eventObj.endDate}`, //"2019-03-15T14:00:00",
        timeZone: "Turkey Standard Time",
      },
      location: {
        displayName: `${eventObj.location}`,
      },
      attendees: [
        //{
        //  emailAddress: {
        //    address: "mehmet.dindar@experto.com.tr",
        //    name: "Adele Vance",
        //  },
        //  type: "required",
        //},
      ],
      isOnlineMeeting: eventObj.isOnline,
      onlineMeetingProvider: "teamsForBusiness",
    };

    eventObj.katilimcilar.map((k) => {
      eventParam.attendees.push({
        emailAddress: {
          address: k.mail,
          name: k.adi,
        },
        type: "required",
      });
    });

    const c = await axios.patch(
      `https://graph.microsoft.com/v1.0/users/${fromMsId}/events/${eventObj.toplantimsid}`,
      eventParam,
      { headers: { Authorization: "Bearer " + respLogin.data.access_token } }
    );
    console.log(`Toplantı guncellendi gonderildi`);
  } catch (e) {
    console.log(`Toplanti guncelleme hata `, e);
  }
};

module.exports = {
  createEvent,
  updateEvent,
};
