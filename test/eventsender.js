const amqplib = require("amqplib/callback_api");
const amqp = process.env.AMQP_URL;
const event_queue = process.env.QUEUE_EVENT;

amqplib.connect(amqp, (err, connection) => {
  if (err) {
    console.error(err.stack);
    return process.exit(1);
  }

  // Create channel
  connection.createChannel((err, channel) => {
    if (err) {
      console.error(err.stack);
      return process.exit(1);
    }

    // Ensure queue for messages
    channel.assertQueue(
      event_queue,
      {
        // Ensure that the queue is not deleted when server restarts
        durable: true,
      },
      (err) => {
        if (err) {
          console.error(err.stack);
          return process.exit(1);
        }

        let sender = (content) => {
          let sent = channel.sendToQueue(
            event_queue,
            Buffer.from(JSON.stringify(content)),
            {
              // Store queued elements on disk
              persistent: true,
              contentType: "application/json",
            }
          );
          //if (sent) {
          //  return next();
          //} else {
          //  channel.once("drain", () => next());
          //}
        };

        sender({
          fromMsId: "1c88de8b-4d0d-4f59-817c-61f2b76438f0",
          toMsId: "1c88de8b-4d0d-4f59-817c-61f2b76438f0", //"38a80f08-cd9f-4467-8a52-63bd859a4119",
          eventObj: {
            _id: '5f809c9fe7edaee7400b292d',
            subject: "Test " + new Date(),
            content: "content",
            startDate: new Date(), // "2020-10-15T12:00:00",
            endDate: "2020-10-15T14:00:00",
            location: "yer",
            isOnline: true,
            katilimcilar: [{ mail: "mehmet.dindar@gmail.com", adi: "MSD" }],
          },
        });


        sender({
          fromMsId: "1c88de8b-4d0d-4f59-817c-61f2b76438f0",
          toMsId: "1c88de8b-4d0d-4f59-817c-61f2b76438f0", //"38a80f08-cd9f-4467-8a52-63bd859a4119",
          eventObj: {
            _id: '5f809c9fe7edaee7400b292d',
            toplantimsid: 'AAMkAGY5YjZlZmI1LTE3MmItNDNhZi05MTI1LWM5OTMxYjMxNzQxNgBGAAAAAABSQIbP43zzTK7247ZWRTzVBwDLvhf7ngYxRKvl562QzBF7AAAAAAENAADLvhf7ngYxRKvl562QzBF7AAEgWVFVAAA=',
            subject: "Test " + new Date(),
            content: "content",
            startDate: new Date(), // "2020-10-15T12:00:00",
            endDate: "2020-10-15T14:00:00",
            location: "yer",
            isOnline: true,
            katilimcilar: [{ mail: "mehmet.dindar@gmail.com", adi: "MSD" }],
          },
        });
        /*
        // Create a function to send objects to the queue
        // Javascript object is converted to JSON and then into a Buffer
        let sender = (content, next) => {
          console.log(JSON.stringify(content));
          let sent = channel.sendToQueue(
            event_queue,
            Buffer.from(JSON.stringify(content)),
            {
              // Store queued elements on disk
              persistent: true,
              contentType: "application/json",
            }
          );
          if (sent) {
            return next();
          } else {
            channel.once("drain", () => next());
          }
        };

        // push 100 messages to queue
        let sent = 0;
        let sendNext = () => {
          if (sent >= 1) {
            console.log("All messages sent!");
            // Close connection to AMQP server
            // We need to call channel.close first, otherwise pending
            // messages are not written to the queue
            return channel.close(() => connection.close());
          }
          sent++;
          updater();
          sender(
            {
              fromMsId: "1c88de8b-4d0d-4f59-817c-61f2b76438f0",
              toMsId: "1c88de8b-4d0d-4f59-817c-61f2b76438f0", //"38a80f08-cd9f-4467-8a52-63bd859a4119",
              eventObj: {
                subject: "Test",
                content: "content",
                startDate: "2020-10-15T12:00:00",
                endDate: "2020-10-15T14:00:00",
                location: "yer",
                isOnline: true,
                katilimcilar: [{ mail: "mehmet.dindar@gmail.com", adi: "MSD" }],
              },
            },
            sendNext
          );
        };

        sendNext();
        */
      }
    );
  });
});
