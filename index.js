const amqplib = require("amqplib/callback_api");
const Event = require("./event");
const Mail = require("./mail");
const initMongo = require("./config/mongo");

const amqp = process.env.AMQP_URL;
const mail_queue = process.env.QUEUE_MAIL;
const event_queue = process.env.QUEUE_EVENT;

initMongo();

amqplib.connect(amqp, (err, connection) => {
  if (err) {
    console.error(err.stack);
    return process.exit(1);
  }

  connection.createChannel((err, channel) => {
    if (err) {
    }
    channel.assertQueue(mail_queue, { durable: true }, (err) => {
      if (err) {
      }
      channel.prefetch(1);
      channel.consume(mail_queue, (data) => {
        if (data === null) {
          return;
        }
        console.log("MAIL");
        let message = JSON.parse(data.content.toString());
        //console.log(message);
        Mail.sendMail(message.fromMsId, message.toMsId, message.mailObj, message.mailObj.cc).then(
          () => {
            return channel.ack(data);
          }
        );
      });
    });
  });

  connection.createChannel((err, channel) => {
    if (err) {
      console.error(err.stack);
      return process.exit(1);
    }
    channel.assertQueue(event_queue, { durable: true }, (err) => {
      if (err) {
        console.error(err.stack);
        return process.exit(1);
      }

      channel.prefetch(1);
      channel.consume(event_queue, (data) => {
        if (data === null) {
          return;
        }

        console.log("EVENT");
        // Decode message contents
        let message = JSON.parse(data.content.toString());
        //console.log(message.eventObj);
        //let k = JSON.parse(message.eventObj.katilimcilar)
        //console.log(k)

        if (message.eventObj.toplantimsid) {
          Event.updateEvent(
            message.fromMsId,
            message.toMsId,
            message.eventObj
          ).then((r) => {
            return channel.ack(data);
          });
        } else {
          Event.createEvent(
            message.fromMsId,
            message.toMsId,
            message.eventObj
          ).then((r) => {
            return channel.ack(data);
          });
        }

        // attach message specific authentication options
        // this is needed if you want to send different messages from
        // different user accounts
        //message.auth = {
        //  user: "testuser",
        //  pass: "testpass",
        //};

        // Send the message using the previously set up Nodemailer transport
        /*
              transport.sendMail(message, (err, info) => {
                if (err) {
                  console.log("Olmadi");
                  console.error(err.stack);
                  // put the failed message item back to queue
                  //return channel.nack(data);
                  return channel.ack(data)
                }
                console.log("Delivered message %s", info.messageId);
                // remove message item from the queue
                channel.ack(data);
              });
              */
      });
    });
  });
});
