const axios = require("axios");
var querystring = require("querystring");

const tenantId = process.env.TENANT_ID
const clientId = process.env.CLIENT_ID
const clientSecret = process.env.CLIENT_SECRET

const sendMail = async (fromMsId, toMsId, mailObj, ccMsId) => {
  try {
    const respLogin = await axios.post(
      `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
      querystring.stringify({
        client_id: clientId,
        scope: "https://graph.microsoft.com/.default",
        client_secret: clientSecret,
        grant_type: "client_credentials",
      }),
      {
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
      }
    );

    const to = await axios.get(
      `https://graph.microsoft.com/v1.0/users/${toMsId}`,
      { headers: { Authorization: "Bearer " + respLogin.data.access_token } }
    );


    //console.log(ccMsId)

    var body = {
      message: {
        subject: `[EXPERTO-APP] ${mailObj.subject}`,
        body: {
          contentType: "HTML",
          content: `${mailObj.content}`,
        },
        toRecipients: [
          {
            emailAddress: {
              address: `${to.data.mail}`,
            },
          },
        ],
        // ccRecipients: [
        //   {
        //     emailAddress: {
        //       address: `${cc ? cc.data.mail : ''}`,
        //     },
        //   },
        // ],
      },
      saveToSentItems: "true",
    }

    if (ccMsId) {
      body.message['ccRecipients'] = []
      for (let cc of ccMsId) {
        let _cc = await axios.get(
          `https://graph.microsoft.com/v1.0/users/${cc}`,
          { headers: { Authorization: "Bearer " + respLogin.data.access_token } }
        );
        if (_cc) {
          body.message.ccRecipients.push(
            {
              emailAddress: {
                address: _cc.data.mail,
              }
            }
          )
        }
      }
    }

    const c = await axios.post(
      `https://graph.microsoft.com/v1.0/users/${fromMsId}/sendMail`,
      body,
      { headers: { Authorization: "Bearer " + respLogin.data.access_token } }
    );

    //logger.debug.info(`Mail Gonderildi: [UserID: ${fromMsId}] [Data: ${to.data.mail}]`)

    console.log(`Mail gonderildi`);
  } catch (e) {
    console.log(e)
    //logger.debug.error(`Mail Gonderilemedi: [UserID: ${fromMsId}] [Data: ${JSON.stringify(e)}]`)
  }
};

const sendMailToMulti = async (fromMsId, expertoList, firmaList, mailObj, attachment) => {
  try {
    const respLogin = await axios.post(
      `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
      querystring.stringify({
        client_id: clientId,
        scope: "https://graph.microsoft.com/.default",
        client_secret: clientSecret,
        grant_type: "client_credentials",
      }),
      {
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
      }
    );

    var toRecipients = [];

    // Experto katilimcilarinin mail adresleri toplanir
    for (var expertoUser of expertoList) {
      const to = await axios.get(
        `https://graph.microsoft.com/v1.0/users/${expertoUser}`,
        { headers: { Authorization: "Bearer " + respLogin.data.access_token } }
      );
      toRecipients.push({
        emailAddress: {
          address: `${to.data.mail}`,
        },
      });
    }

    // Firma katilimcilarinin mail adresleri toplanir
    for (var firmaUser of firmaList) {
      if (firmaUser.mail) {
        toRecipients.push({
          emailAddress: {
            address: `${firmaUser.mail}`,
          },
        });
      }
    }

    const c = await axios.post(
      `https://graph.microsoft.com/v1.0/users/${fromMsId}/sendMail`,
      {
        message: {
          subject: `[EXPERTO-APP] ${mailObj.subject}`,
          body: {
            contentType: "HTML",
            content: `${mailObj.content}`,
          },
          toRecipients: toRecipients,
          attachments: [
            {
              '@odata.type': "#microsoft.graph.fileAttachment",
              "name": "ToplantiNotu.pdf",
              "contentBytes": attachment
            }
          ]
        },
        saveToSentItems: "true",
      },
      { headers: { Authorization: "Bearer " + respLogin.data.access_token } }
    );
    //logger.debug.info(`Mail Gonderildi: [UserID: ${fromMsId}] [Data: ${JSON.stringify(toRecipients, null, 4)}]`)
    console.log(`Mailler gonderildi`);
  } catch (e) {
    //logger.debug.error(`Mail Gonderilemedi: [UserID: ${req.tokenUser.uniqueId}] [Data: ${JSON.stringify(e)}]`)
  }
};

module.exports = {
  sendMail,
  sendMailToMulti
};
